#ifndef __IAP_H__
#define __IAP_H__
#include "sys.h"  
#include "bsp.h"

typedef  void (*iapfun)(void);				//定义一个函数类型的参数.
								
#define FIRMWARE_BUFF_ADDR (FRAMEBUF+ 480*272*4)  		 //固件buf在两倍的LCD Buff之后

void iap_load_app(uint32_t appxaddr);			//执行flash里面的app程序
void iap_init(void);
uint8_t iap_check_file(void);
uint8_t iap_check_update(void);
void iap_update_fimware(void);
void disable_used_irq(void);
#endif







































