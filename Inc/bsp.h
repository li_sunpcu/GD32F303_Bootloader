#ifndef __BSP_H
#define __BSP_H

#include "gd32f30x.h"

#define FLASH_PAGE_SIZE         ((uint32_t)0x00000800)   /* FLASH Page Size:2K */
#define FLASH_APP1_ADDR		0x08010000  	//第一个应用程序起始地址(存放在FLASH)										
                                      	//保留0X08000000~0X0800FE00的空间为IAP使用
#define CRC_FLASH_ADDR  (FLASH_APP1_ADDR - FLASH_PAGE_SIZE)  //IAP 空间最后一页保存CRC校验值
#define FLASH_APP1_END_ADDR 	FLASH_USER_START_ADDR		
#define FLASH_USER_START_ADDR   ((uint32_t)0x08010000 + 0x30000 - 0x1800)   /* Start @ of user Flash area; GD32F207ZC 256K flash ���6K��Ϊ�û����� */
#define FLASH_USER_END_ADDR     ((uint32_t)0x08010000 + 0x30000)   /* End @ of user Flash area */

#define LED1_PIN                         GPIO_PIN_15
#define LED1_GPIO_PORT                   GPIOB
#define LED1_GPIO_CLK                    RCU_GPIOB
#define LED1_ON()                        GPIO_BOP(LED1_GPIO_PORT) = LED1_PIN
#define LED1_OFF()                       GPIO_BC(LED1_GPIO_PORT) = LED1_PIN


#define USART0_CLK                    RCU_USART0
#define USART0_TX_PIN                 GPIO_PIN_9
#define USART0_TX_GPIO_PORT           GPIOA
#define USART0_RX_PIN                 GPIO_PIN_10
#define USART0_RX_GPIO_PORT           GPIOA
#define USART0_TX_GPIO_CLK            RCU_GPIOA
#define USART0_RX_GPIO_CLK            RCU_GPIOA


#define UID_ADDR           0x1FFFF7E8


void bsp_init(void);
void delay_us(uint32_t us);
void delay_ms(uint32_t ms);
void UART_Send_Buf(uint32_t uartx, uint8_t *buf, uint16_t size);
uint16_t UART3RecvData(uint8_t *pbuf);
void UART_Printf(uint32_t uartx, const char* fmt, ...);
uint8_t writeToFlash(char* databuf, uint32_t writelength, uint32_t targetflashaddress);
uint8_t erase_flash( uint32_t writelength, uint32_t targetflashaddress);
void iap_load_app(uint32_t appxaddr);
#endif//__BSP_H
