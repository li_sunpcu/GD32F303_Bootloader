/*!
    \file  main.c
    \brief led spark with systick, USART print and key example
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-02-10, V1.0.0, firmware for GD32F30x
*/


#include "systick.h"
#include "main.h"
#include "bsp.h"
#include "log.h"
#include "iap.h"
/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/

int main(void)
{
    bsp_init();
    log_info("BSP Init OK!!");
    iap_init();
		iap_check_file();
    if(iap_check_update())
    {
        if(((*(__IO uint32_t*)(FLASH_APP1_ADDR+4))&0xFF000000)==0x08000000 )//判断是否为0X08XXXXXX.
        {	
            log_info("Find available APP,Jump to APP!");
            iap_load_app(FLASH_APP1_ADDR);//执行FLASH APP代码
        }
        else log_info("No available APP,entry loop!");
    }else{
        log_info("start update firmwate.");
        iap_update_fimware();
    }

    while (1){
        led_spark();
        delay_1ms(200);
    }
}

